<?php

    function open_database_connection() {
        
        //opens database connection
        $link = new PDO('mysql:host=localhost;dbname=quiz','root','root');
        return $link;
    }

    function close_database_connection($link) {
        
        //closes database connections
        $link = NULL;
    }

    function logincheck() {
        // checks the admin login
        $link = open_database_connection();
        $stmt = $link->prepare("SELECT * FROM admins WHERE username=:username AND passwords=:passwords");
        $stmt->bindParam(":username", $_POST['name']);
        $stmt->bindParam(":passwords", $_POST['password']);
        $t = $stmt->execute();
        close_database_connection($link);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo $row['username'];
    }

    function loginparticipant() {
        // partcipant login
        $link = open_database_connection();
        $stmt2 = $link->prepare("SELECT COUNT(name) FROM participant where name=:name");
        $stmt2->bindParam(":name", $_POST['name']);
        $t = $stmt2->execute();
        $row = $stmt2->fetch(PDO::FETCH_ASSOC);
        $count = $row['COUNT(name)'];
        if(empty($count)){
            $stmt = $link->prepare("INSERT INTO participant(name, emailid) VALUES(:name,:emailid)");
            $stmt->bindParam(":name", $_POST['name']);
            $stmt->bindParam(":emailid", $_POST['emailid']);
            $t = $stmt->execute();
            close_database_connection($link);
            $_SESSION['name'] = $_POST['name'];
            $_SESSION['emailid'] = $_POST['emailid'];
        }    
        else {
            $_SESSION['name'] = $_POST['name'];
            $_SESSION['emailid'] = $_POST['emailid'];
            close_database_connection($link);
        }
    }

    function getquestions() {
        // function for getting the questions form the database
        $link = open_database_connection();
        $stmt = $link->prepare("SELECT question,answer,optiona,optionb,optionc,optiond FROM questions");
        $t = $stmt->execute();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data[]=$row;
        }
        return $data;
        close_database_connection($link);
    }

    function getmax() {

        $link = open_database_connection();
        $stmt = $link->prepare("SELECT MAX(id) from questions");
        $t = $stmt->execute();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data[]=$row;
        }
        return $data;
        close_database_connection($link);
    }

    function getanswer() {
        $link = $link = open_database_connection();
        $stmt = $link->prepare("SELECT * FROM questions WHERE question=:question AND answer=:answer");
        $stmt->bindParam(":question", $_SESSION['question']);
        $stmt->bindParam(":answer", $_POST['option']);
        $t = $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
        close_database_connection($link);
    }
    
    function insertmarks() {
        $link = $link = open_database_connection();
        $stmt = $link->prepare("INSERT INTO participantscore(participantname, mark) VALUES(:participantname,:mark)");
        $stmt->bindParam(":participantname", $_SESSION['name']);
        $stmt->bindParam(":mark", $_SESSION['mark']);
        $t = $stmt->execute();
        close_database_connection($link);
    }
    
    function getask($q, $max, $questions) {
        $link = $link = open_database_connection();
        $stmt2 = $link->prepare("SELECT COUNT(question) FROM askedquestions WHERE question=:question AND participantname=:participantname");
        $stmt2->bindParam(":question", $_SESSION['question']);
        $stmt2->bindParam(":participantname", $_SESSION['name']);
        $t = $stmt2->execute();
        $row = $stmt2->fetch(PDO::FETCH_ASSOC);
        $count = $row['COUNT(question)'];
        close_database_connection($link);
        
        if(empty($count)){
            $link = $link = open_database_connection();
            $stmt = $link->prepare("INSERT INTO askedquestions(participantname, question) VALUES(:participantname,:question)");
            $stmt->bindParam(":participantname", $_SESSION['name']);
            $stmt->bindParam(":question", $_SESSION['question']);
            $t = $stmt->execute();
            close_database_connection($link);
        }

        else {
            $rand =rand(0 ,$max);
            $q = $questions[$rand]['question'];
            getask($q, $max, $questions);
        }
        
    }

?>    