<?php

require_once "controller.php";
require_once "model.php";

session_start();

$uri = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);

if("/index.php" == $uri) {

    userpage();
}

elseif("/index.php/user/login" == $uri) {
    
    //login the user
    loginuser();
}

elseif("/index.php/user/login/questions/shuffle/q" == $uri) {
    
    //display questions login
    $max = 0;
    showquestions();
}

elseif("/index.php/user/login/questions/shuffle" == $uri) {
    
    //login the user
    shufflearray();
}



elseif("/index.php/user/login/testcompleted" == $uri) {
    
    //display admin login
   testcompleted();
}


elseif("/index.php/admin" == $uri) {

    //display admin page after login
    show_adminpage();
}

elseif("/index.php/admin/login" == $uri) {
    
    //display admin homepage 
    loginadmin();
    echo "hello";
    }

elseif("/index.php/user/logout" == $uri) {
        
    //destroy session 
    session_destroy();
    unset($_SESSION['question']);
    unset($_SESSION['name']);
    unset($_SESSION['emailid']);
    header("location:http://test.local/index.php");
}

?>