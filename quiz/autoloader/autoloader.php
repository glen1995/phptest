<?php 

spl_autoload_register(function($classname) {
    
    $filename = "/controller/".strtolower($classname)."."."php";
    if(file_exists($filename)){
        require_once $filename."php";
    }

    $filename = "/model/".strtolower($classname)."php";
    if(file_exists($filename)){
        require_once $filename."php";
    }

})


?>