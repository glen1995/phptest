<?php 

    function show_adminpage() {
        
        //shows the admin login page
        include "templates/admin.tpl.php";
    }

    function loginadmin() {
        //check the admin
        logincheck();
    }

    function userpage() { 
        //shows the user registration form
        include "templates/user.tpl.php";
    }

    function loginuser() {

        loginparticipant();
        include "templates/take_the_test.tpl.php";
    }
    
    function shufflearray() {
        $questions = getquestions();
        $_SESSION['quest'] = $questions;
        $m = getmax();
        $max = $m[0]['MAX(id)'];
        for($i = 0; $i<$max ; $i++ ) {
            $arr[] = $i;
        }
        $arra=shuffle($arr);
        $count = $_POST['count'];
        if(isset($_SESSION['question'])) {
            
            $anwser = getanswer();

            if(!empty($anwser)) {
                $mark = $_POST['mark'] + 1;
            }
            else {
                $mark = $_POST['mark'];
            }
        }
        $c=0;
        $i=0;
        $rand = $arr[$c];
        $options = array("optiona", "optionb", "optionc", "optiond");
        $d = shuffle($options);
        include "templates/questions.tpl.php";
    }

    /*function showquestionsd() {
        $count = $_POST['count'];
        if(isset($_SESSION['question'])) {
            
            $anwser = getanswer();

            if(!empty($anwser)) {
                $mark = $_POST['mark'] + 1;
            }
            else {
                $mark = $_POST['mark'];
            }
        }
        $questions = getquestions();
        $m = getmax();
        $max = $m[0]['MAX(id)'];
        $arra = $_POST['arra'];
        $arr = explode(",",$arra);
        $i = $_POST['ival'];
        $rand = $arr[$i];
        include "templates/questions.tpl.php";
       
    }*/

    function showquestions() {
        $count = $_POST['count'];
        if(isset($_SESSION['question'])) {
            
            $anwser = getanswer();

            if(!empty($anwser)) {
                $mark = $_POST['mark'] + 1;
            }
            else {
                $mark = $_POST['mark'];
            }
        }
        $questions = getquestions();
        $m = getmax();
        $max = $m[0]['MAX(id)'];
        $arra = $_POST['arra'];
        $arr = explode(",",$arra);
        $i = $_POST['ival'];
        $rand = $arr[$i];
        include "templates/questions.tpl.php";
       
    }

    function testcompleted() {
        // after the test is completed
        $mark = $_SESSION['mark'];
        insertmarks();
        include "templates/testcompleted.tpl.php";
    }


?>