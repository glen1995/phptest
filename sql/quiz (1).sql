-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 02:54 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `passwords` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `passwords`) VALUES
(1, 'glen', '1');

-- --------------------------------------------------------

--
-- Table structure for table `askedquestions`
--

CREATE TABLE `askedquestions` (
  `id` int(11) NOT NULL,
  `participantname` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'physics'),
(2, 'php');

-- --------------------------------------------------------

--
-- Table structure for table `mark`
--

CREATE TABLE `mark` (
  `id` int(11) NOT NULL,
  `partid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL,
  `emailid` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `name`, `emailid`) VALUES
(1, 'df', 'df'),
(3, 'glen', 'dsf'),
(14, 'sd', 'as'),
(29, 'glen', 'glen123'),
(30, 'glen', 'glen123'),
(31, 'jithin', 'jith'),
(32, 'ret', 'rew'),
(33, 'fg', 'vcb'),
(34, 'hjj', 'cvx'),
(35, 'hjj', 'cvx'),
(36, 'fg', 'dg'),
(37, 'hg', 'jk'),
(38, 'ty', 'hj'),
(48, 'bn', 'vbn'),
(49, 'as', 'sudheesh@gmail.com'),
(50, 'bcv', 'vbn'),
(51, 'cv', 'vbc'),
(52, 'cxv', 'sf'),
(53, 'gel', 'cxv'),
(54, 'cvb', 'asdf'),
(55, 'nv', 'nbv'),
(56, 'vbc', 'm,n'),
(57, 'vcb', 'vbn'),
(58, 'xcv', 'vbc'),
(59, 'vb', 'bvn'),
(60, 'vbn', 'vbn'),
(61, 'bcvb', 'cvb'),
(62, 'xc', 'vc'),
(63, 'cbv', 'bnv'),
(64, 'fdg', 'dg'),
(65, 'bvn', 'gfd'),
(66, 'vnb', 'fgh'),
(67, 'sdf', 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `participantscore`
--

CREATE TABLE `participantscore` (
  `id` int(11) NOT NULL,
  `participantname` varchar(255) NOT NULL,
  `mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participantscore`
--

INSERT INTO `participantscore` (`id`, `participantname`, `mark`) VALUES
(27, 'as', 3),
(28, 'xcv', 3),
(29, 'cbv', 3),
(30, 'bvn', 1),
(31, 'vbn', 2),
(32, 'bn', 1),
(33, 'vnb', 3),
(34, 'sdf', 2),
(35, 'sdf', 2),
(36, 'sdf', 2),
(37, 'sdf', 2),
(38, 'sdf', 2),
(39, 'sdf', 2),
(40, 'glen', 10);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `optiona` varchar(250) NOT NULL,
  `optionb` varchar(250) NOT NULL,
  `optionc` varchar(250) NOT NULL,
  `optiond` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `categoryid`, `question`, `answer`, `optiona`, `optionb`, `optionc`, `optiond`) VALUES
(1, 1, 'what did newton find', 'gravity', 'gravit', 'gravity', 'gravi', 'grav'),
(2, 1, 'who founded tesla', 'tesla', 'tesl', 'tesla', 'tes', 'te'),
(3, 2, 'best backend language', 'php', 'php', 'ph', 'p', 'phpp'),
(4, 2, 'what is drupal?', 'framework', 'framewo', 'framewor', 'framework', 'framew');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `askedquestions`
--
ALTER TABLE `askedquestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mark`
--
ALTER TABLE `mark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participantscore`
--
ALTER TABLE `participantscore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `askedquestions`
--
ALTER TABLE `askedquestions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mark`
--
ALTER TABLE `mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `participantscore`
--
ALTER TABLE `participantscore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
